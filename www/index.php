<?php
declare(strict_types=1);
namespace SqlStringParser;
require 'autoload.php';
?>
<p><a href='/dump.php'>Імпорт БД</a></p>
<p>Доступний метод search</p>
<p>Перевіряються по можливості поля в таблиці</p>
<p>[дія search] [таблиця] [умова where ...] [order таблиця] [limit]</p>
<p><b>Приклад запитy</b></p>
<p>seArch     users
    wHere naMe="Yura" and date_register="one day ago" and age>=20 order name limit 20;</p>

<form>
    <textarea name="str" placeholder="INPUT SQL STRING" style="width:100%"><?=$_REQUEST['str']?></textarea>
    <p><input type="submit"></p>
</form>

<?php
if($_REQUEST['str']){
    $sql = $_REQUEST['str'];
}else{
    $sql = 'seArch     users
    wHere naMe="Yura" and date_register="one day ago" and age>=20 order name limit 20;';
}

$parser = new SqlStringParser($sql);
$builder = $parser->parse();

$tree = $builder->buildTree($builder->tree,0);
$formatSql = $builder->buildSql();

?>
<h3>Форматований вивід</h3>
<?=$formatSql?>
<h3>AST</h3>
<pre><?php print_r($tree);?></pre>
<h3>Результат</h3>
<pre><?php print_r($builder->db->query($formatSql));?></pre>
