<?php
declare(strict_types=1);
namespace SqlStringParser;
require 'autoload.php';
$db = new \SqlStringParser\Database\Database();
$db->importDump();
header('Location: //'.$_SERVER['HTTP_HOST']);
?>
