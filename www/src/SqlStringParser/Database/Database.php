<?php
namespace SqlStringParser\Database;

class Database{
    private $host = 'mysql';
    private $db = 'png';
    private $user = 'root';
    private $password = 'tiger';

    public function __construct()
    {
        try {
            $dbh = new \PDO('mysql:host='.$this->host.';dbname='.$this->db, $this->user, $this->password);
            $this->dbh = $dbh;

        }catch (\PDOException $e){
            die('Не вдалося підключитися до бази даних');
        }
    }

    public function importDump()
    {
        if(file_exists($_SERVER['DOCUMENT_ROOT'].'/dump.sql')){
            $this->dbh->query(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/dump.sql'));
        }
    }

    public function query($query):array {
        try {
            $this->dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $stmt = $this->dbh->prepare($query);
            $stmt->execute();
            $rows = $stmt->fetchAll();
            return $rows;
        } catch(\PDOException $e) {
            die($e->getMessage());
        }
    }
}
?>
