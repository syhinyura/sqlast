<?php
namespace SqlStringParser\Builders;
use SqlStringParser\Helpers\Reserved;
use SqlStringParser\Database\Database;
use SqlStringParser\Validators\Validator;

class SearchBuilder{

        public $tokens;
        public $keyWordsLevelAdd = ['where','('];
        public $keyWordsLevelMinus = [')'];
        public $keyWordsOperators = ['>','-','+','\\','*','=','!=','>=','<=','<>'];
        public $tree,$temp = [];

        public function __construct($tokens)
        {
            $this->tokens = $tokens;
            $this->temp = array(
                'currentNodeID'=>0,
                'currentParentID'=>0,
                'previousParentID'=>0,
                'depthLevel'=>1
            );
            $this->db = new Database();
        }

        public function groupTokens()
        {
            foreach ($this->tokens as $key=>$token){
                $this->temp['currentNodeID']++;

                if(in_array(strtolower($token),$this->keyWordsLevelAdd)){
                    $this->temp['previousParentID'] = $this->temp['currentParentID'];
                    $this->addChildren($token);
                    $this->addLevel();
                    $this->temp['currentParentID'] = $this->temp['currentNodeID'];

                }elseif(in_array(strtolower($token),$this->keyWordsLevelMinus)){
                    $this->removeLevel();
                    $this->temp['currentParentID'] = $this->temp['previousParentID'];
                    $this->addChildren($token);
                }
                else{
                    $this->addChildren($token);
                }
            }

            unset($this->temp);
            $this->groupTreeByOperators($this->tree);
            return $this;
        }

        public function addLevel(){
            $this->temp['depthLevel']++;
        }

        public function removeLevel(){
            $this->temp['depthLevel']--;
        }

        public function addChildren($value){
            $this->tree[] = array(
                "ID"=>$this->temp['currentNodeID'],
                "LEVEL"=>$this->temp['depthLevel'],
                'VALUE'=>$value,
                'PARENT_ID'=>$this->temp['currentParentID']
            );
        }

        public function groupTreeByOperators(array &$elements) {
            foreach ($elements as $key=>&$element) {
                if(in_array($element['VALUE'],$this->keyWordsOperators)){
                    if($elements[$key+1]['VALUE'] != ')' &&
                        $elements[$key+1]['VALUE'] != '(' &&
                        $elements[$key-1]['VALUE'] != ')' &&
                        $elements[$key-1]['VALUE'] != '('
                    ){
                        $elements[$key+1]['PARENT_ID'] = $element['ID'];
                        $elements[$key-1]['PARENT_ID'] = $element['ID'];
                    }

                }
            }
        }

        public function fillOptions():SearchBuilder{
            $tree = $this->tree;
            $this->temp['tree'] = [];
            $tempTables = array();

            while(true){
                if(count($tree) <= 0){
                    break;
                }
                foreach ($tree as $key=>&$element){
                    $currentValue = strtolower($element['VALUE']);
                    $nextValue = strtolower($tree[$key+1]['VALUE']);
                    $this->temp['currentElement'] = $element;
                    switch($currentValue){
                        case 'search':
                            $this->addOptions('SELECT','SELECT *FROM');
                            unset($tree[$key]);
                            if(!in_array($nextValue,Reserved::$allowTables)){
                                die('Таблиця не знайдена або некоректний запит!');
                            }
                            $this->temp['currentElement'] = $tree[$key+1];
                            $tempTables[] = $nextValue;
                            $this->addOptions('TABLE',$nextValue);
                            if(!$this->getTableFields($nextValue)){
                                $this->fillTableFields($this->db->query('DESCRIBE '.$nextValue),$nextValue);
                            }
                            unset($tree[$key+1]);
                            break 2;

                        case 'where':
                            $this->addOptions('WHERE',Reserved::$keyWordsRelation[$currentValue]);
                            break;
                        case 'count':
                            $regexStr = array_map(function($arr){
                                return $arr['VALUE'];
                            }, $tree);
                            $regexStr = implode('{$}',$regexStr);
                            $result = preg_match('/^count{\$}of{\$}('.implode('|',Reserved::$allowTables).'){\$}/U',$regexStr,$matches);
                            if(!$result){
                                die('Некоректний запит!');
                            }
                            $this->addOptions('COUNT','SELECT COUNT(*) FROM '.$matches[1]);
                            unset($tree[$key]);
                            unset($tree[$key+1]);
                            unset($tree[$key+2]);
                            break 2;
                        case '(':
                        case ')':
                            $this->addOptions('BRACKET',$currentValue);
                            break;
                        case '>':
                        case '<':
                        case '!=':
                        case '=':
                        case '>=':
                        case '<=':
                        case '<>':
                            $this->addOptions('OPERATORS',$currentValue);
                            break;
                        default:

                            if(!empty(Reserved::$keyWordsRelation[$currentValue])){
                                $this->addOptions('KEYWORDS',Reserved::$keyWordsRelation[$currentValue]);
                                break;
                            }elseif (in_array($currentValue, Reserved::$operatorsRelation)){
                                $this->addOptions('MATH_OPERATORS',$currentValue);
                                break;
                            }

                            //Поле
                            if($this->formatFieldValue($currentValue,$tempTables)){
                                break;
                            }

                            //Число значення
                            if(is_numeric($currentValue)){
                                $this->addOptions('VALUE',$currentValue);
                                break;
                            }

                            //Текст значення
                            if(strpos($currentValue,'"' ) !== false ||
                                strpos($currentValue,"'" ) !== false){
                                $this->addOptions('VALUE',$currentValue);
                            }else{
                                if(in_array($currentValue,['delete','alter'])){
                                        die('Операції не дозволені');
                                }
                                //Значення які не передбачили
                                $this->addOptions('NOT_FOUND_TYPE',$currentValue);
                            }
                            break;
                    }
                    unset($tree[$key]);
                }
            }
            $this->tree = $this->temp['tree'];
            $this->temp = [];
            return $this;
        }

        public function addOptions($type,$value){
            if($type == 'VALUE'){
                $nameField =$this->temp['tree'][count($this->temp['tree'])-2];
                if($nameField['OPTIONS']['TYPE'] == 'FIELD' && $nameField['OPTIONS']['FIELD_TYPE']){
                    $fieldType=$nameField['OPTIONS']['FIELD_TYPE'];
                    switch ($fieldType){
                        case 'date':
                            $validator = new Validator($fieldType,$value);
                            $value = $validator->validate();
                            break;
                            //Інші валідатори

                    }
                }
            }

            $element = $this->temp['currentElement'];
            $element['OPTIONS']['TYPE'] = $type;
            $element['OPTIONS']['SQL_VALUE'] = $value;
            $this->temp['tree'][] = $element;
        }

        public function fillTableFields($fields,$table){
            $this->db->fields[$table] = $fields;
        }

        public function getTableFields($table){
            if(!empty($this->db->fields[$table])){
                return $this->db->fields[$table];
            }else{
                return false;
            }
        }

        public function formatFieldValue($currentField,$tables):bool{
            $tables = array_reverse($tables);
            $type = '';
            foreach ($tables as $table){
                foreach ($this->db->fields[$table] as $field){
                    if($currentField == $field['Field']){
                        $type = $field['Type'];
                        break 2;
                    }

                }
            }

            if($type){
                $this->temp['currentElement']['OPTIONS']['FIELD_TYPE'] = $type;
                $this->addOptions('FIELD',$currentField);
                return true;
            }else{
                return false;
            }
        }

        public function buildTree(array &$elements, $parentId = 0) {
            $branch = array();
            foreach ($elements as &$element) {

                if ($element['PARENT_ID'] == $parentId) {
                    $children = $this->buildTree($elements, $element['ID']);
                    if ($children) {
                        $element['children'] = $children;
                    }
                    $branch[$element['ID']] = $element;
                    unset($element);
                }
            }
            return $branch;
        }

        public function buildSql():string {
            $sql = '';
            foreach ($this->tree as $key=>$element) {
                if($element['OPTIONS']['SQL_VALUE']){
                    $sql .= $element['OPTIONS']['SQL_VALUE'].' ';
                }else{
                    $sql .= $element['VALUE'].' ';
                }
            }
           return $sql;
        }

    }
?>
