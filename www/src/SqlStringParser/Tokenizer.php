<?php
namespace SqlStringParser;
use SqlStringParser\Builders\SearchBuilder;
use SqlStringParser\Helpers\Reserved;
class Tokenizer{
    public $splitters = ["<=>", "\r\n", "!=", ">=", "<=", "<>", "<<", ">>", ":=", "\\", "&&", "||", ":=",
        "/*", "*/", "--", ">", "<", "|", "=", "^", "(", ")", "\t", "\n", "'", "\"", "`",
        ",", "@", " ", "+", "-", "*", "/", ";"];
    public function process($string){
        $tokens = preg_split($this->convertSplittersToRegexPattern($this->splitters), $string, null, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        $tokens = $this->searchQuotes($tokens);
        $tokens = $this->deleteWhiteSpace($tokens);
        return $this->getBuilder($tokens);
    }

    protected function getBuilder($tokens)
    {
        $builder = strtolower($tokens[0]);
        if(in_array($builder,Reserved::$availableAction)){
            switch($builder){
                case 'search':
                    $searchBuilder = new SearchBuilder($tokens);
                    $builder = $searchBuilder->groupTokens()->fillOptions();
                    return $builder;
                    break;
            }
        }else{
            die('Дія неможлива!');
        }
    }

    protected function deleteWhiteSpace($tokens):array {
        $cnt = count($tokens);
        $i = 0;
        while ($i < $cnt) {
            $token = $tokens[$i];
            if (1 == preg_match('/^\s+/', $token)) {
                unset($tokens[$i]);
            }
            $i++;
        }
        return array_values($tokens);
    }
    protected function isQuotes($token):bool {
        return ($token === "'" || $token === "\"" || $token === "`");
    }

    protected function searchQuotes($tokens):array {
        $i = 0;
        $cnt = count($tokens);
        while ($i < $cnt) {

            if (!isset($tokens[$i])) {
                $i++;
                continue;
            }

            $token = $tokens[$i];

            if ($this->isQuotes($token)) {
                $tokens = $this->balanceCharacter($tokens, $i, $token);
            }

            $i++;
        }

        return $tokens;
    }

    protected function balanceCharacter($tokens, $idx, $char):array {

        $token_count = count($tokens);
        $i = $idx + 1;
        while ($i < $token_count) {

            if (!isset($tokens[$i])) {
                $i++;
                continue;
            }

            $token = $tokens[$i];
            $tokens[$idx] .= $token;
            unset($tokens[$i]);

            if ($token === $char) {
                break;
            }

            $i++;
        }
        return array_values($tokens);
    }

    public function convertSplittersToRegexPattern( $splitters ):string {
        $regex_parts = array();
        foreach ( $splitters as $part ) {
            $part = preg_quote( $part );
            switch ( $part ) {
                case " ":
                    $part = '\s';
                    break;
                case "/":
                    $part = "\/";
                    break;
                case "/\*":
                    $part = "\/\*";
                    break;
                case "\*/":
                    $part = "\*\/";
                    break;
            }

            $regex_parts[] = $part;
        }

        $pattern = implode( '|', $regex_parts );

        return '/(' . $pattern . ')/';
    }
}
