<?php
namespace SqlStringParser;

class SqlStringParser
{
    public $tokens;

    public function __construct($string)
    {
        if ($this->checkEmpty($string)) {
            $this->string = $string;
        }
    }

    protected function checkEmpty($string){
        $string = trim($string);
        if($string){
            return true;
        }else{
            die('Строка пуста!');
        }
    }
    public function parse(){
        $tokenizer = new Tokenizer();
        $builder = $tokenizer->process($this->string);
        return $builder;
    }
}