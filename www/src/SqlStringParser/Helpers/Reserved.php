<?php
namespace SqlStringParser\helpers;
class Reserved
{
    public static $availableAction = [
        'search',
        //change
        //delete
    ];
    public static $operatorsRelation = [
        '-',
        '*',
        '+',
        '/',
        '%'
    ];

    public static $keyWordsRelation = [
        'search'=>"SELECT",
        'where'=>"WHERE",
        'and'=>"AND",
        'or'=>"OR",
        'not'=>"NOT",
        'beetween'=>"BETWEEN",
        'order'=>"ORDER BY",
        'limit'=>"LIMIT",
    ];

    public static $allowTables = [
        'users','posts'
    ];
}
?>
