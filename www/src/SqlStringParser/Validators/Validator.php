<?php
namespace SqlStringParser\Validators;

class Validator{
    protected $numberWords = [
        1 => "one",
        2 => "two",
        3 => "three",
        4 => "four",
        5 => "five",
        6 => "six",
        7 => "seven",
        8 => "eight",
        9 => "nine",
        10 => "ten",
        11 => "eleven",
        12 => "twelve",
        13 => "thirteen",
        14 => "fourteen",
        15 => "fifteen",
        16 => "sixteen",
        17 => "seventeen",
        18 => "eighteen",
        19 => "nineteen"
    ];
    public function __construct($type,$value)
    {
        $this->type = $type;
        $this->value = str_replace(array('"',"'"),'',$value);
        $this->numberWords = array_flip($this->numberWords);
    }
    public function validate()
    {
        $validator = $this->type;
        return $this->$validator();
    }
    public function date() : string
    {
        $formatValue = $tempValue = $this->value;
        if($this->value != '1970-01-01'){
            $tempDate = date('Y-m-d',strtotime($tempValue));
            //Змогли переконвертувати
            if($tempDate!='1970-01-01'){
                $formatValue = '"'.$tempDate.'"';
            }else{
                //Шукаємо можливі варіанти
                $tempValue = strtolower($tempValue);
                $result = preg_match('~([a-z]+)\s+(days|day|months|month|year|years)\s+(ago)~',$tempValue,$matches);
                if($result){
                    if($matches[3] == 'ago'){
                        $tempDate = date('Y-m-d',strtotime('-'.$this->numberWords[$matches[1]].' '.$matches[2]));
                    }
                }
                $formatValue = '"'.$tempDate.'"';
            }
        }
        return $formatValue;
    }
}

?>
