<?php
spl_autoload_register(function ($class) {
    $base_dir = './src/';
    $file = $base_dir . str_replace('\\', '/', $class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});